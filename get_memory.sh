#!/bin/bash
username=${1?Error: no username given}
rm -rf output.log
while true; do
        myvar=$(ps -U $username --no-headers -o rss | awk '{ sum+=$1} END {print int(sum/1024) }')
        echo $myvar >> output.log
        echo $myvar
        sleep 1
done

